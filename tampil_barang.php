<?php 
//---------------------------------------//
session_start();
    if(empty($_SESSION['username']))
    {
        header("location:index.php");
    }
//---------------------------------------//

require 'config/koneksi.php';
$barang = query("SELECT * FROM tb_barang");

//kolom pencarian terisi
if(isset($_POST["cari"])){
    $barang = cari($_POST["keyword"]);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inventory Gudang</title>
    <link rel="stylesheet" href="assets/css/tbarang.css">

    <!-- Datatable style -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

    <!-- Jquery -->
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

</head>
<body>
    
    <h2 align=center>Daftar Barang</h2>
    <center>
        <table>
            <form action="form_barang.php" method="POST">
                <button>Tambah Barang</button>
            </form>
            <form action="menu.php" method="POST">
                <button>Menu Utama</button>
            </form>
            <form action="logout.php" method="POST">
                <button>LogOut</button>
            </form>
        </table>
        <br>

        <form action="" method="POST">
            <input type="text" name="keyword" size="40" autofocus placeholder="masukkan keyword pencarian.." autocomplete="off" id="keyword">
        </form>

        <div id="container">
            <table class="styled-table" border=0> 
                <tr class="judul">
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Brand</th>
                    <th>Kategori</th>
                    <th>Jumlah</th>
                    <th>Harga</th>
                    <th>Gambar</th>
                    <th>Action</th>
                </tr>
                    <?php $i = 1; ?>
                    <?php foreach($barang as $row) { ?>
                    <tr class="isi">
                        <td align=center><?= $i; ?> </td>
                        <td align=left><?= $row["namabrg"] ?> </td>
                        <td align=left><?= $row["brand"] ?> </td>
                        <td align=left><?= $row["kategori"] ?> </td>
                        <td align=center><?= $row["jumlah"] ?> </td>
                        <td align=left>Rp. <?= $row["harga"] ?> </td>
                        <td align=center> <img src=gambar/<?= $row["gambar"]; ?> width="70" height="70"> </td>

                        <td align=center><a style="text-decoration: none" href="edit_barang.php?id=<?php echo $row['idbarang']; ?>" >Edit</a>
                        <br> <br>
                        <a style="text-decoration: none" href="hapus_barang.php?id=<?php echo $row['idbarang']; ?>" onclick="return confirm('Yakin ingin menghapus data ini?')">Hapus </a>
                        
                         
                        </td>
                    </tr>

                    <?php $i++;
                     } ?>
            </table>
        </div>

    </center>
            <script src="assets/js/cari.js"></script>

</body>
</html>